TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    slltestsuite.cpp

HEADERS += \
    singlylinkedlist.h \
    singlylinkedlist.tpp \
    slltestsuite.h
