#ifndef SINGLYLINKEDLIST_H
#define SINGLYLINKEDLIST_H

template<typename T>
class SinglyLinkedList
{
private:

    struct node {
        T value;
        struct node *next;
    };

    struct node *head;
    int numElements = 0;
    /*
     * Hide your node structures here:
     */


public:
    /*
     * Here comes the public API!
     */
    SinglyLinkedList();

    /*
     * Adds an element so that it becomes the new list head
     */
    void addToFront(const T &element);

    /*
     * Adds an element so that it becomes the new list tail
     */
    void addToBack(const T &element);

    /*
     * Returns true if the list has no elements
     */
    bool isEmpty();

    /*
     * Returns the number of elements in the list
     */
    int size();

    /*
     * These operate on indices for inserting, removing and getting
     * the element at index 'idx', respectively
     */
    void insertElementAt(int idx, const T &element);
    void removeElementAt(int idx);
    const T& getElementAt(int idx);

    /*
     * This is a simple iterator inner class.
     * It is used to hold the current position in the list while
     * the user traverses it.
     */
    class Iterator {
        friend class SinglyLinkedList<T>;
    private:
        /*
         * Pointer to the list that we are iterating over
         */
        SinglyLinkedList<T> *parent;
        struct node* current;

        Iterator(SinglyLinkedList<T> *l) {
            parent = l;
            current = parent->head;
        }

    public:
        /*
             * Get the element at he position of this iterator
             */
        const T& get();

        /*
             * Advance iterator by one position
             */
        void next();

        /*
             * Returns true if calling next will cause us to be
             * over a valid element in the list.
             * False if the next element is NULL
             */
        bool hasNext();
    };

    /*
     * Make a new iterator that starts with the head element
     * Shouldn't have to change this..
     */
    Iterator begin() {
        Iterator it(this);
        return it;
    }

};


#include "singlylinkedlist.tpp"

#endif // SINGLYLINKEDLIST_H
