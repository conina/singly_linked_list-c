#include "slltestsuite.h"
#include "singlylinkedlist.h"

bool SLLTestSuite::creationTest()
{
    SinglyLinkedList<int> list;
    return true;
}

bool SLLTestSuite::addSingleElementTest()
{
    SinglyLinkedList<int> list;
    list.addToFront(1);
    MY_ASSERT(list.getElementAt(0) == 1);
    return true;
}

bool SLLTestSuite::addMultipleElementsTest()
{
    SinglyLinkedList<int> list;

    list.addToFront(3);
    list.addToFront(2);
    list.addToFront(1);

    MY_ASSERT(list.getElementAt(0) == 1);
    MY_ASSERT(list.getElementAt(1) == 2);
    MY_ASSERT(list.getElementAt(2) == 3);

    return true;
}

bool SLLTestSuite::removalTest()
{
    SinglyLinkedList<int> list;

    list.addToFront(3);
    list.addToFront(2);
    list.addToFront(1);

    list.removeElementAt(1);

    MY_ASSERT(list.getElementAt(0) == 1);
    MY_ASSERT(list.getElementAt(1) == 3);

    return true;
}

bool SLLTestSuite::insertionTest()
{
    SinglyLinkedList<int> list;

    list.insertElementAt(0,3);

    list.addToFront(3);
    list.addToFront(2);
    list.addToFront(1);

    list.insertElementAt(1, 4);

    // Hope this is right ;)
    MY_ASSERT(list.getElementAt(0) == 1);
    MY_ASSERT(list.getElementAt(1) == 4);
    MY_ASSERT(list.getElementAt(2) == 2);
    MY_ASSERT(list.getElementAt(3) == 3);
    MY_ASSERT(list.getElementAt(4) == 3);

    return true;
}

bool SLLTestSuite::sizeTest()
{
    SinglyLinkedList<int> list;

    list.addToFront(3);
    list.addToFront(2);
    list.addToFront(1);

    MY_ASSERT(list.size() == 3);

    list.insertElementAt(1, 4);
    MY_ASSERT(list.size() == 4);

    list.removeElementAt(1);
    MY_ASSERT(list.size() == 3);

    list.removeElementAt(5);
    MY_ASSERT(list.size() == 3);

    return true;
}

bool SLLTestSuite::iteratorTest()
{
    SinglyLinkedList<int> list;

    list.addToFront(7);
    list.addToFront(6);
    list.addToFront(5);
    list.addToFront(4);
    list.addToFront(3);
    list.addToFront(2);
    list.addToFront(1);
    list.addToFront(0);

    SinglyLinkedList<int>::Iterator it = list.begin();
    for (int i = 0; i < 7; i++) {
        MY_ASSERT(it.get() == i);
        it.next();
    }
    MY_ASSERT(it.get() == 7);
    MY_ASSERT(! it.hasNext());

    return true;
}

bool SLLTestSuite::addTailTest()
{
    SinglyLinkedList<int> list;

    list.addToFront(1);
    list.addToBack(2);

    MY_ASSERT(list.getElementAt(0) == 1);
    MY_ASSERT(list.getElementAt(1) == 2);

    return true;
}

SLLTestSuite::SLLTestSuite()
{
    std::cout << "====== Running test suite ======" << std::endl;
    RUN_TEST(creationTest);
    RUN_TEST(addSingleElementTest);
    RUN_TEST(addMultipleElementsTest);
    RUN_TEST(addTailTest);
    RUN_TEST(removalTest);
    RUN_TEST(insertionTest);
    RUN_TEST(sizeTest);
    RUN_TEST(iteratorTest);
    std::cout << "================================" << std::endl;
}
