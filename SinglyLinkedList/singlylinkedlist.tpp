#include "singlylinkedlist.h"
#include <stdlib.h> // Added to get the NULL definition

/*
 * List class method definitions:
 */
template <typename T>
SinglyLinkedList<T>::SinglyLinkedList()
{
    head = NULL;
}

template<typename T>
void SinglyLinkedList<T>::addToFront(const T &element)
{
    struct node *newElement = new struct node;
    newElement->value = element;
    newElement->next = head;
    head = newElement;
    numElements++;


}

template<typename T>
void SinglyLinkedList<T>::addToBack(const T &element)
{
    struct node *newElement = new struct node;
    struct node *nodeIterator = head;

    while (nodeIterator->next != NULL) {
        nodeIterator = nodeIterator->next;
    }

    nodeIterator->next = newElement;
    newElement->value = element;
    newElement->next = NULL;
    numElements++;
}

template<typename T>
const T &SinglyLinkedList<T>::getElementAt(int idx)
{
    struct node *nodeIterator = head;
    for (int i = 0; i < idx; i++) {
        nodeIterator = nodeIterator->next;
    }
    return nodeIterator->value;
}

template<typename T>
void SinglyLinkedList<T>::insertElementAt(int idx, const T &element)
{
    struct node *newElement = new struct node;
    struct node *insertionSpot = head;

    if (idx == 0) {
        addToFront(element);
        numElements++;
        return;
    }

    if (idx >= size()) {
        std::cout << "Index not valid.";
        return;
    }

    for (int i = 0; i < idx - 1; i++) {
        insertionSpot = insertionSpot->next;
    }

    newElement->next = insertionSpot->next;
    newElement->value = element;
    insertionSpot->next = newElement;
    numElements++;
}


template<typename T>
void SinglyLinkedList<T>::removeElementAt(int idx)
{
    if (idx >= size()) {
        std::cout << "Index not valid.";
        return;
    }

    struct node *beforeDeletionSpot = head;

    for (int i = 0; i < idx - 1; i++) {
        beforeDeletionSpot = beforeDeletionSpot->next;
    }

    struct node *delitionSpot = beforeDeletionSpot->next;
    beforeDeletionSpot->next = delitionSpot->next;
    numElements--;
}

template<typename T>
bool SinglyLinkedList<T>::isEmpty()
{
    return (numElements == 0);
}

template<typename T>
int SinglyLinkedList<T>::size()
{
    return numElements;
}

/*
 * Iterator method definitions:
 */
template<typename T>
const T& SinglyLinkedList<T>::Iterator::get()
{
    return current->value;
}

template<typename T>
void SinglyLinkedList<T>::Iterator::next()
{
    if (hasNext()) {
        current = current->next;
    }
}

template<typename T>
bool SinglyLinkedList<T>::Iterator::hasNext()
{
    return current->next != NULL;
}
