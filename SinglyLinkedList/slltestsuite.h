#ifndef SLLTESTSUITE_H
#define SLLTESTSUITE_H

#include <iostream>

#define RUN_TEST(function) \
    std::cout << "Running test '" << #function << "': ";\
    std::cout << (function() ? "PASSED!" : "FAILED!") << std::endl;\


#define MY_ASSERT(expr) \
    if (!(expr)) return false;

class SLLTestSuite
{
private:
    bool creationTest();
    bool addSingleElementTest();
    bool addMultipleElementsTest();
    bool addTailTest();
    bool removalTest();
    bool insertionTest();
    bool sizeTest();
    bool iteratorTest();
public:
    SLLTestSuite();
};

#endif // SLLTESTSUITE_H
